#!/bin/sh

# Preparació inicial de la imatge per poder modificar-la.
# Partim d'una imatge iso oficial com a paràmetre.

if [ $# -ne 1 ]; then
  echo "Cal passar la ruta al fitxer iso de la imatge original."
  exit 1
fi

isoimage=$1

# Fem una copia editable de la imatge
mkdir loopdir
mount -o loop $isoimage loopdir
mkdir cd
rsync -a -H --exclude=TRANS.TBL loopdir/ cd
umount loopdir
rmdir loopdir

# Fem copia de seguretat dels fitxers que modificarem
cp cd/install.amd/initrd.gz .
