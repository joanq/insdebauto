#!/bin/bash

# Passos a realitzar després de la instal·lació

# Creació d'altres usuaris
adduser --quiet super sudo
# Atenció: cal canviar password_alum-01 per la contrasenya d'alum-01
echo -e "password_alum-01\npassword_alum-01" | adduser --quiet --gecos "alum-01,,," alum-01
# Atenció: cal canviar password_coord per la contrasenya de coord
echo -e "password_coord\npassword_coord" | adduser --quiet --gecos "coord,,," coord
adduser --quiet coord sudo

# Atom
wget -qO - https://packagecloud.io/AtomEditor/atom/gpgkey | apt-key add -
sh -c 'echo "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main" > /etc/apt/sources.list.d/atom.list'
# Virtualbox
echo "deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian/ bullseye contrib" >> /etc/apt/sources.list
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | apt-key add -
# Instal·lació
export DEBIAN_FRONTEND=noninteractive
unset  DEBIAN_HAS_FRONTEND
unset  DEBCONF_REDIR
unset  DEBCONF_OLD_FD_BASE
apt-get update
apt-get -qq -y --allow-unauthenticated install atom
apt-get -qq -y --allow-unauthenticated install virtualbox-6.1
