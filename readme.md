# Instal·lador automàtic de Debian 11

## Contingut del dipòsit

- `init.sh`: script que crea, a partir d'una imatge ISO de Debian oficial,
una còpia editable al directori `cd`.
- `make_image.sh`: script que genera una imatge ISO a partir dels continguts del
directori `cd` i dels fitxers `adtxt.cfg`, `preseed.cfg` i `postinstall.sh`.
- `adtxt.cfg`: fitxer que controla la configuració d'arrencada de l'instal·lador
automàtic al GRUB de l'instal·lador.
- `pressed.cfg`: configuració de l'instal·lador automàtic. Aquí hi ha totes les
respostes que es passen a l'instal·lador de Debian.
- `postinstall.sh`: script que es copia a la imatge ISO final i que s'executarà
com a pas final de la instal·lació ja en el sistema instal·lat.

## Prerequisits

- `xorriso`
- `isolinux`

## Passos per crear un instal·lador automatitzat

1. Baixar una imatge oficial.
2. Executar `init.sh <imatge.iso>`. Amb això generem la còpia editable a `cd`.
3. Modificar, si cal, el fitxer `adtxt.cfg`. Per exemple, canviar la `priority`
depenent de quines preguntes volem que surtin.
4. Modificar el fitxer `preseed.cfg` i/o el fitxer `postinstall.sh`.
5. Executar `make_image.sh`. Amb això generem la imatge
`debian-autoinstall.iso`.
6. Provar l'instal·lador generat.

Repetir els passos 4, 5 i 6 per cada modificació que vulguem provar.

**Cal modificar `password_root` i `password_super` al fitxer `preseed.cfg` per
les contrasenyes de `root` i `super` respectivament. També cal canviar
`password_alum-01` i `password_coord` a `postinstall.sh` per les contrasenyes
d'`alum-01` i `coord`.**

La imatge ISO que es genera es pot utilitzar tant per un CD/DVD com per una clau
USB.

Per guardar-la a una clau USB n'hi ha prou amb:

```
# cat debian-autoinstall.iso > /dev/sdx
```

on `sdx` és el disc corresponent a la clau USB.

Per arrencar l'instal·lador automàtic, al menú d'arrencada de l'instal·lador,
cal seleccionar `Advanced options` i `Automated install`.

## Notes

**Amb les opcions per defecte, l'instal·lador generat elimina totes les dades
del disc dur sense preguntar**.

## Possibles problemes

### Falta el firmware per la targeta de xarxa.

En aquest cas, l'instal·lador no pot anar a buscar el firmware adequat perquè
no pot establir connexió a Internet.

Baixa el [paquet de firmware no lliure](http://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stretch/current/firmware.tar.gz)
de la web de Debian.

Copia'l i descomprimeix-lo a una altra clau USB.

Quan instal·lis, connecta tant la clau USB amb l'instal·lador com la clau que
conté el firmware.

L'instal·lador hauria de detectar i agafar el firmware que falta automàticament.
En alguns casos pot ser que calgui passar a consola i muntar la clau USB a mà.

Veure [6.4. Loading Missing Firmware](https://www.debian.org/releases/stable/amd64/ch06s04.html.en).

Un cop passat aquest punt de la instal·lació, no cal esperar per extreure l'USB
amb el firmware, que ja es pot utilitzar per arrencar el següent ordinador a
instal·lar.

### L'instal·lador no és capaç d'esborrar el disc sencer.

En alguns esquemes de particions EFI, l'instal·lador automàtic no esborra el
disc i crea les seves particions automàticament.

En aquest cas, es pot arrencar l'instal·lador normal, arribar fins el punt de
particionat de disc, i seleccionar `Utilitza el disc sencer`.

Un cop esborrat el disc, ja podem reiniciar i arrencar l'instal·lador automàtic.

## Demostració

[Vídeo demostració](https://vimeo.com/276931893)
