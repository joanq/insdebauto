#!/bin/sh

# Un cop modificat el fitxer preseed.cfg, l'afegim al initrd de l'instal·lador

# Opcions d'arrencada
cp adtxt.cfg cd/isolinux/adtxt.cfg
# Restaurem fitxer original
cp initrd.gz cd/install.amd/initrd.gz
# Afegim preseed.cfg
gunzip cd/install.amd/initrd.gz
echo "preseed.cfg" | cpio -o -H newc -A -F cd/install.amd/initrd
gzip cd/install.amd/initrd
# Copiem postinstall.sh
cp postinstall.sh cd/
# Regenerem les sumes de comprovació dels paquets
cd cd
md5sum `find -follow -type f` > md5sum.txt
# Generem la nova imatge
xorriso -as mkisofs -o ../debian-autoinstall.iso \
 -isohybrid-mbr /usr/lib/ISOLINUX/isohdpfx.bin \
 -c isolinux/boot.cat -b isolinux/isolinux.bin \
 -no-emul-boot -boot-load-size 4 -boot-info-table .
cd ..
